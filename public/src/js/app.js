var deferredPrompt;

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('/sw.js').then(function(registration) {
        // Registration was successful
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
      }).catch(function(err) {
        // registration failed :(
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }

  window.addEventListener('beforeinstallprompt', (event)=>{
    console.log('beforeinstallprompt fired');
    event.preventDefault();
    deferredPrompt = event;
    return false;
  });

  var promise = new Promise((resolve, reject)=>{
    setTimeout(function(){
      // resolve('timer is done')
      reject({code:500, message: 'An error ocurred!!'});
      // console.log('timer is done');
    }, 3000);
  });

  fetch('https://api.chucknorris.io/jokes/random').then((data)=>{
    console.log(data);

    return data.json();
  }).then((data)=>{
    console.log(data.value);
  }).catch((err)=>{
    console.log(err);
  });


  fetch('https://httpbin.org/post', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    mode:'cors',
    body: JSON.stringify({
      message: 'HOLA mundo'
    })
  }).then((data)=>{
    console.log(data);

    return data.json();
  }).then((data)=>{
    console.log(data);
  }).catch((err)=>{
    console.log(err);
  });
  
  // promise.then((text)=>{
  //   return text;
  // }).then((newText)=>{
  //   console.log(newText);
  // });
  
  promise.then((text)=>{
    return text;
  }).then((newText)=>{
    console.log(newText);
  }).catch((err)=>{
    console.log(err.code, err.message);
  });

  console.log('set time out');