
/**
 * En l0os service worker tenemos funcionaes especiales para correr en segundo plano
 * 
 * el objeto (self) sirve para la llamada de ventana y se esta pendiente de un evento
 */
self.addEventListener('install', (event)=>{
  console.log('Se instalo el service worker', event);
});

self.addEventListener('activate', (event)=>{
  console.log('Se activo el service worker', event);
  return self.clients.claim();
});

//va a cargar todos los activos
self.addEventListener('fetch', (event)=>{
  console.log('Se FETCHING EL SERVICE WORKER ..', event);
  event.respondWith(fetch(event.request));
});

